import os
import unittest
from flask import current_app
from datetime import datetime
from app import create_app, db
from app.models import Tenant
from app.main.data_ingestion import loadTenants
from collections import Counter

class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])

    def create_dummy_records_with_particular_lease_start_date(self):
        t1 = Tenant(property_name='Queenswood Heights',
                    property_add_01='Queenswood Heights',
                    property_add_02='Queenswood Gardens',
                    property_add_03='Headingley',
                    property_add_04='Leeds',
                    unit_name='Queenswood Hgt-Telecom App.',
                    tenant_name='Vodafone Ltd',
                    lease_start_date=datetime.strptime('01 Jun 1999', '%d %b %Y'),
                    lease_end_date=datetime.strptime('07 Nov 2029', '%d %b %Y'),
                    lease_years=25,
                    current_rent=1000)

        t2 = Tenant(property_name='Beecroft Hill',
                    property_add_01='Broad Lane',
                    property_add_02='',
                    property_add_03='',
                    property_add_04='LS13',
                    unit_name='Beecroft Hill - Telecom App',
                    tenant_name='Arqiva Services ltd',
                    lease_start_date=datetime.strptime('31 Aug 2007', '%d %b %Y'),
                    lease_end_date=datetime.strptime('28 Feb 2058', '%d %b %Y'),
                    lease_years=64,
                    current_rent=23950)

        db.session.add_all([t1, t2])
        db.session.commit()

    def test_tenant_creation(self):

        self.create_dummy_records_with_particular_lease_start_date()

        tenants = Tenant.query.all()

        self.assertEqual(len(tenants), 2)

    def test_group_by_name(self):

        filename = "/Python_Developer_Test_Dataset_NEW.csv"
        loadTenants(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

        tenants = Tenant.query.order_by(Tenant.current_rent.asc()).all()

        tenant_names = [tenant.tenant_name for tenant in tenants]
        current_app.logger.debug("tenant_names {}".format(tenant_names))
        tenant_dict = dict(Counter(tenant_names))
        current_app.logger.debug("tenant_dict {}".format(tenant_dict))
        sorted_names = {k: v for k, v in sorted(tenant_dict.items(), key=lambda item: item[1], reverse=True)}
        current_app.logger.debug("sorted_names {}".format(sorted_names))

        self.assertGreater(len(sorted_names),0)

    def test_list_between_start_date(self):
        filename = "/Python_Developer_Test_Dataset_NEW.csv"
        loadTenants(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

        tenants = Tenant.query.order_by(Tenant.current_rent.asc()).\
            filter(Tenant.lease_start_date.
            between(datetime(1999, 6, 1),
            datetime(2007, 8, 31))).all()

        current_app.logger.debug("tenants {}".format(len(tenants)))
        self.assertEqual(len(tenants), 5)


    def test_list_between_start_date_inclusive(self):
        filename = "/Python_Developer_Test_Dataset_NEW.csv"
        loadTenants(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        self.create_dummy_records_with_particular_lease_start_date()

        tenants = Tenant.query.order_by(Tenant.current_rent.asc()).\
            filter(Tenant.lease_start_date.
            between(datetime(1999, 6, 1),
            datetime(2007, 8, 31))).all()

        current_app.logger.debug("lease start dates are inclusive")

        current_app.logger.debug("tenants {}".format(len(tenants)))
        self.assertEqual(len(tenants), 7)