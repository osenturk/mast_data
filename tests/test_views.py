import re
import io
import unittest
from app import create_app, db, fake
from app.models import Tenant
from flask import url_for

class FlaskClientTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        fake.tenants(50)
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_home_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'<!DOCTYPE html>' in response.data)

    def test_uploading_tenants(self):

        data = {}
        data['file'] = (io.BytesIO(b"abcdef"), 'abc.csv')

        response = self.client.post(
            '/upload_file', data=data, follow_redirects=True,
            content_type='multipart/form-data'
        )

        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertIn(b'File successfully uploaded', response.data)

    def test_removing_tenants(self):
        response = self.client.get('/clean_tenants')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'Tenants successfully removed' in response.data)

    def test_menu_page(self):
        response = self.client.get('/menu')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'List the data with Lease Start' in response.data)

    def test_list_tenants_page(self):
        response = self.client.post('/list_tenants/5')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'5 tenants listed ascending order by current rent' in response.data)

    def test_list_lease_years_page(self):
        response = self.client.post('/list_lease_years/25')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'Tenants within 25 years listed' in response.data)

    def test_group_by_name_page(self):
        response = self.client.post('/group_by_name')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'Tenants grouped by their names and sorted' in response.data)

    def test_list_between_start_date_page(self):
        response = self.client.post('/list_between_start_date')
        self.assertEqual(response.status_code, 200)
        # self.app.logger.debug("response.data {}".format(response.data))
        self.assertTrue(b'Lease start date between 1st Jun 1999 and 31st Aug 2007' in response.data)