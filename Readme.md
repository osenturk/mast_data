# Mast Data

## Structure
This is one of the most scalable structures where application factory pattern applied
to create multiple instances and apply different configuration based on specific environments.

1. app directory contains all necessary implementation.
   - main directory composed of data ingestion, errors, and views
   - static directory is used for file uploads
   - templates include the static html files within jinja scripts
   - fake.py helps to create fake data which is useful to test data models
   and pagination
   - __init__.py is where the library initialization implemented with application
   factory pattern
   - models.py represents the data model
2. migrations support data migrations including database initialization, 
upgrades and downgrades
3. 'config.py' contains configurations for each environment
4. requirements are useful to ensure required libraries are installed accordingly.
5. tests contain unittests
6. .flaskenv helps to keep environment variables ready
7. mast_manager.py is useful to access flask shell within application context to
conduct comphrensive tests


     ```bash 
    (venv) ➜  mast_data git:(dev) ✗ flask shell
    Python 3.7.3 (default, Mar 27 2019, 16:54:48) 
    [Clang 4.0.1 (tags/RELEASE_401/final)] on darwin
    App: app [production]
    Instance: /Users/ozansenturk/Downloads/Python/mast_data/instance
    >>> 
     
    ```

```bash
├── Readme.md
├── __pycache__
│   ├── config.cpython-37.pyc
│   └── mast_manager.cpython-37.pyc
├── app
│   ├── __init__.py
│   ├── __pycache__
│   ├── fake.py
│   ├── main
│   ├── models.py
│   ├── static
│   └── templates
├── config.py
├── data-dev.sqlite
├── mast_manager.py
├── migrations
│   ├── README
│   ├── __pycache__
│   ├── alembic.ini
│   ├── env.py
│   ├── script.py.mako
│   └── versions
├── requirements
│   ├── common.txt
│   └── dev.txt
├── tests
│   ├── __init__.py
│   ├── __pycache__
│   ├── test_basics.py
│   └── test_views.py
├── tmp
│   └── coverage
└── venv
    ├── bin
    ├── include
    ├── lib
    └── pyvenv.cfg

```
## How to run:

1. open a command window and follow the steps below

    ```python
    python3 -m venv venv
    
    source venv/bin/activate
    
    pip install -r requirements/dev.txt
    
    flask db init 
    
    flask db migrate -m "database initialization"
    
    flask db upgrade
    
    flask run
    ```
2. go to the url 'http://127.0.0.1:5000/'


    ```bash
    venv) ➜  mast_data git:(dev) flask run
     * Serving Flask app "mast_manager.py" (lazy loading)
     * Environment: production
       WARNING: This is a development server. Do not use it in a production deployment.
       Use a production WSGI server instead.
     * Debug mode: on
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
     * Restarting with stat
     * Debugger is active!
     * Debugger PIN: 118-816-598
    ```
3. click upload link, point the required csv data set, then click upload.
You will see 'Tenants successfully loaded' and the tenants sorted in ascending
order by current rent

4. click the menu link to see the demonstration of the 4 requirements

## Tests
Current test coverage is %60

1. to execute only tests
    
    -
    ```bash
      flask test
      ```
2. to execute tests with coverage

    
    ```bash
    flask test --coverage
   
    Ran 14 tests in 2.472s
    
    OK
    Coverage Summary:
    Name                         Stmts   Miss Branch BrPart  Cover
    --------------------------------------------------------------
    app/__init__.py                 34     24      6      1    28%
    app/fake.py                     16      2      2      0    89%
    app/main/data_ingestion.py      25      8      4      0    72%
    app/main/views.py               71     25     14      4    66%
    --------------------------------------------------------------
    TOTAL                          146     59     26      5    60%
```
```

## Constraints

1. #### Data Model
    Given that the un-normalised data is accepted as individual tenants in the 
    objective section, the multiple property columns were left as they are in the 
    data model instead of creating a separate 'property' data model to present one 
    to many relationship for the mast data. 
    
    Therefore, there is only one table which is not normalized contains 
    duplicated data. Also, if there is a need of adding a 5th property for
    a tenant, it will be more expensive to add a whole column to the table
    compared to add only a record into a separate property table if it have been normalized. 

2. #### Data Ingestion
    Inside the data ingestion the name of columns e.g. 'Property Name', 'Property Address [1]' 
    are referenced directly same as in the csv file. In case column name has
    changed then the implementation should be updated accordingly.


Your further questions, feel free to reach me out via my
email 'ozan.senturk@gmail.com'