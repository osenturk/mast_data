from random import randint
from sqlalchemy.exc import IntegrityError
from faker import Faker
from app import db
from app.models import Tenant

def tenants(count=100):
    fake = Faker()
    i = 0
    while i < count:
        t1 = Tenant(property_name=fake.street_name(),
                    property_add_01=fake.address(),
                    property_add_02=fake.address(),
                    property_add_03=fake.address(),
                    property_add_04=fake.address(),
                    unit_name=fake.company(),
                    tenant_name=fake.name(),
                    lease_start_date=fake.date_between(start_date='-30y', end_date='-2y'),
                    lease_end_date=fake.date_between(start_date='-1y', end_date='today'),
                    lease_years=randint(1, 100),
                    current_rent=randint(1, 10000))
        db.session.add(t1)
        try:
            db.session.commit()
            i += 1
        except IntegrityError:
            db.session.rollback()