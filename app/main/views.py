import os
from app.models import Tenant
from app.main import bp
from flask import render_template,request, url_for,current_app,flash, redirect
from werkzeug.utils import secure_filename
from .data_ingestion import loadTenants, cleanTenants
from collections import Counter
from datetime import datetime


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    page = request.args.get('page', 1, type=int)
    tenants = Tenant.query.order_by(Tenant.current_rent.asc())\
        .paginate(page, current_app.config['TENANT_PER_PAGE'], False)

    next_url = url_for('main.index', page=tenants.next_num) \
        if tenants.has_next else None
    prev_url = url_for('main.index', page=tenants.prev_num) \
        if tenants.has_prev else None

    return render_template('index.html', tenants=tenants.items,
                           next_url=next_url, prev_url=prev_url,
                           page=page, per_page=current_app.config['TENANT_PER_PAGE'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']

@bp.route('/upload_file', methods=['GET', 'POST'])
def upload_file():

    current_app.logger.debug("uploading file section")

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            current_app.logger.debug("file name is {}".format(filename))

            file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            flash('File successfully uploaded')
            loadTenants(filename)
            flash('Tenants successfully loaded')
            return redirect(url_for('main.index'))
        else:
            flash('Allowed file type is csv')
            return redirect(request.url)

    return render_template('upload.html')

@bp.route('/clean_tenants', methods=['GET', 'POST'])
def clean_tenants():
    cleanTenants()
    flash('Tenants successfully removed')
    return render_template('index.html')

@bp.route('/menu', methods=['GET', 'POST'])
def menu():
    return render_template('menu.html',title='Menu')

@bp.route('/list_tenants/<limit>', methods=['GET', 'POST'])
def list_tenants(limit):

    tenants = Tenant.query.order_by(Tenant.current_rent.asc()).limit(limit).all()

    flash('5 tenants listed ascending order by current rent')
    return render_template('index.html', tenants=tenants)


@bp.route('/list_lease_years/<year>', methods=['GET', 'POST'])
def list_lease_years(year):
    page = request.args.get('page', 1, type=int)
    tenants = Tenant.query.order_by(Tenant.current_rent.asc()).\
        filter_by(lease_years = 25).\
        paginate(page, current_app.config['TENANT_PER_PAGE'], False)

    tenant_current_rent_list = [tenant.current_rent for tenant in tenants.items]
    total = sum (tenant_current_rent_list)
    flash('Tenants within 25 years listed and their rent summed up via comprehensions')

    next_url = url_for('main.index', page=tenants.next_num) \
        if tenants.has_next else None
    prev_url = url_for('main.index', page=tenants.prev_num) \
        if tenants.has_prev else None

    return render_template('index.html', tenants=tenants.items,next_url=next_url, prev_url=prev_url,
                           total=total, page=page, per_page=current_app.config['TENANT_PER_PAGE'])

@bp.route('/group_by_name', methods=['GET', 'POST'])
def group_by_name():
    tenants = Tenant.query.order_by(Tenant.current_rent.asc()).all()

    tenant_names = [tenant.tenant_name for tenant in tenants]
    tenant_dict = dict(Counter(tenant_names))
    sorted_names = {k: v for k, v in sorted(tenant_dict.items(), key=lambda item: item[1],reverse=True)}

    flash('Tenants grouped by their names and sorted by their number of counts in descending order')

    return render_template('group_by_name.html', names=sorted_names)

@bp.route('/list_between_start_date', methods=['GET', 'POST'])
def list_between_start_date():
    page = request.args.get('page', 1, type=int)
    tenants = Tenant.query.order_by(Tenant.current_rent.asc()). \
        filter(Tenant.lease_start_date.
               between(datetime(1999, 6, 1),
                       datetime(2007, 8, 31)))\
        .paginate(page, current_app.config['TENANT_PER_PAGE'], False)

    next_url = url_for('main.index', page=tenants.next_num) \
        if tenants.has_next else None
    prev_url = url_for('main.index', page=tenants.prev_num) \
        if tenants.has_prev else None

    flash('Lease start date between 1st Jun 1999 and 31st Aug 2007')
    return render_template('index.html', tenants=tenants.items,next_url=next_url, prev_url=prev_url,
                           page=page, per_page=current_app.config['TENANT_PER_PAGE'])