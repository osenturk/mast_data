import os
import csv
from flask import current_app
from app.models import Tenant
from app import db
from datetime import datetime

def loadTenants(filename):

    file_with_path = current_app.config['UPLOAD_FOLDER'] + "/" + filename

    with open(file_with_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)

        count = 0
        bulk_insert_list = []

        for row in reader:

            tenant = Tenant(  property_name =row['Property Name'],
                              property_add_01 =row['Property Address [1]'],
                              property_add_02 =row['Property  Address [2]'],
                              property_add_03 =row['Property Address [3]'],
                              property_add_04 =row['Property Address [4]'],
                              unit_name =row['Unit Name'],
                              tenant_name =row['Tenant Name'],
                              lease_start_date =datetime.strptime(row['Lease Start Date'], '%d %b %Y'),
                              lease_end_date =datetime.strptime(row['Lease End Date'], '%d %b %Y'),
                              lease_years =row['Lease Years'],
                              current_rent =row['Current Rent'])

            bulk_insert_list.append(tenant)
            # current_app.logger.debug("name {}".format(row['Property Name']))

            if (len(bulk_insert_list) % current_app.config['TENANT_PER_PAGE']) == 0 :
                db.session.bulk_save_objects(bulk_insert_list)
                db.session.commit()
                bulk_insert_list.clear()
                current_app.logger.debug(" {} records inserted".format(current_app.config['TENANT_PER_PAGE']))
                # TODO how to insert bulk for each 10 and the rest with all together

        db.session.bulk_save_objects(bulk_insert_list)
        db.session.commit()

def cleanTenants():
    # for all records
    db.session.query(Tenant).delete()
    db.session.commit()

