from app import db
from datetime import datetime
from flask_wtf.file import FileField, FileRequired

class Tenant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    property_name = db.Column(db.String(64))
    property_add_01 = db.Column(db.String(128))
    property_add_02 = db.Column(db.String(128))
    property_add_03 = db.Column(db.String(128))
    property_add_04 = db.Column(db.String(128))
    unit_name = db.Column(db.String(64))
    tenant_name = db.Column(db.String(64))
    lease_start_date = db.Column(db.DateTime)
    lease_end_date = db.Column(db.DateTime)
    lease_years = db.Column(db.Integer)
    current_rent = db.Column(db.Numeric)
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Tenant {} with id: {}, property_name: {}, prop_add_01: {}, ' \
               'prop_add_02: {}, prop_add_03: {}, prop_add_04:{}>' \
               'unit_name: {}, tenant_name: {}, lease_start_date:{}>' \
               'lease_end_date: {}, lease_years: {}, current_rent:{}>' \
               'creation_time: {}' \
            .format(self.id, self.property_name, self.property_add_01,
                    self.property_add_02, self.property_add_03, self.property_add_04,
                    self.unit_name, self.tenant_name, self.lease_start_date,
                    self.lease_end_date, self.lease_years, self.current_rent,
                    self.creation_time)
